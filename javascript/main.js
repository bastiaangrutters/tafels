Learn = function() {};
Learn = Learn.prototype = function() {};

Learn.correctAnswers = 0;
Learn.incorrectAnswers = 0;
Learn.answersPerSet = 11;
Learn.answersingLocked = false;
Learn.start = new Date();

Learn.save = function(key, value) {
    if (typeof (Storage) !== "undefined") {
        localStorage[key] = value;
    }
};

Learn.load = function(key) {
    if (typeof (Storage) !== "undefined" && localStorage[key]) {
        return localStorage[key];
    }

    return null;
};

Learn.updateStats = function(correct, answer, callback, waitForButton) {
    let answerInput = jQuery('#answer');
    let correction = jQuery('#correction');
    let statsCorrect = jQuery('#correct');
    let statsIncorrect = jQuery('#incorrect');
    let statsPercentage = jQuery('#percentage');
    let statsRounds = jQuery('#rounds');
    let statsAverageTime = jQuery('#time');
    let correctAudio = new Audio('audio/sfx_coin_cluster3.wav');
    correctAudio.volume = 0.2;
    let incorrectAudio = new Audio('audio/sfx_sounds_damage1.wav');
    incorrectAudio.volume = 0.2;
    let delay = 500;

    if (correct) {
        answerInput.addClass('correct');
        Learn.correctAnswers++;
        correctAudio.play();
    } else {
        answerInput.addClass('incorrect');
        correction.html(answer).removeClass('hidden');
        Learn.incorrectAnswers++;
        incorrectAudio.play();
        delay = 4000;
    }
    answerInput.blur();
    statsCorrect.html(Learn.correctAnswers);
    statsIncorrect.html(Learn.incorrectAnswers);
    statsPercentage.html(Math.round(Learn.correctAnswers / (Learn.correctAnswers + Learn.incorrectAnswers) * 100));
    statsRounds.html(Math.round((Learn.correctAnswers + Learn.incorrectAnswers + Number.EPSILON) / Learn.answersPerSet * 10) / 10);
    let seconds = (((new Date()).getTime() - Learn.start.getTime()) / 1000);
    statsAverageTime.html(Math.round((seconds / (Learn.correctAnswers + Learn.incorrectAnswers) + Number.EPSILON) * 10) / 10);

    if (!waitForButton) {
        setTimeout(function() {
            correction.addClass('hidden');
            answerInput.val('').focus().removeClass('correct').removeClass('incorrect');
            callback();
        }, delay);
        return;
    }

    setTimeout(function() {
        let stats = jQuery('#stats');
        stats.prepend('<span class="button" id="next">Verder</span>');
        stats.find('#next').on('click', function() {
            jQuery(this).remove();
            correction.addClass('hidden');
            answerInput.val('').focus().removeClass('correct').removeClass('incorrect');
            callback();
        });
    }, delay);
};

Learn.resetStats = function() {
    let statsCorrect = jQuery('#correct');
    let statsIncorrect = jQuery('#incorrect');
    let statsPercentage = jQuery('#percentage');
    let statsRounds = jQuery('#rounds');
    let statsAverageTime = jQuery('#time');

    Learn.correctAnswers = 0;
    Learn.incorrectAnswers = 0;
    Learn.start = new Date();
    statsCorrect.html(Learn.correctAnswers);
    statsIncorrect.html(Learn.incorrectAnswers);
    statsPercentage.html('0');
    statsRounds.html('0');
    statsAverageTime.html('-');
};

Learn.shuffle = function(array) {
    let currentIndex = array.length;
    let temporaryValue;
    let randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
};


jQuery(document).ready(function() {
    Learn.completeAudio = new Audio('audio/applause.wav');
    Learn.completeAudio.volume = 0.2;

    let selectionContainer = jQuery('#selection-container');
    if (selectionContainer.hasClass('word-list-selection')) {
        Learn.initializeWordLists();
    } else {
        Learn.initializeMathTables();
    }
});
