Learn.initializeMathTables = function() {
    let selectionContainer = jQuery('#selection-container');
    let loaded = Learn.load('active');
    if (loaded) {
        selectionContainer.find('.toggle').removeClass('active');
        let loadedActive = loaded.split(',');
        for (let i in loadedActive) {
            if (!loadedActive.hasOwnProperty(i)) {
                continue;
            }

            selectionContainer.find('span[data-base="' + loadedActive[i] + '"]').addClass('active');
        }
    }
    let question = jQuery('#question');

    selectionContainer.find('.toggle').on('click', function() {
        let element = jQuery(this);
        if (element.hasClass('active')) {
            element.removeClass('active');
        } else {
            element.addClass('active');
        }
        let activeList = [];
        selectionContainer.find('.toggle.active').each(function() {
            activeList.push(jQuery(this).data('base'));
        });
        Learn.save('active', activeList.join(','));
        baseNumbers = activeList;
        populateCalculations();
        resetStats();
    });

    let baseNumbers = [];
    selectionContainer.find('.toggle.active').each(function() {
        baseNumbers.push(jQuery(this).data('base'));
    });
    let currentNumber;
    let currentMultiplier;
    let calculations = {};
    let populateCalculations = function() {
        if (baseNumbers.length === 0) {
            return;
        }
        for (let i in baseNumbers) {
            if (!baseNumbers.hasOwnProperty(i)) {
                continue;
            }
            calculations[baseNumbers[i]] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        }
        Learn.answersPerSet = 11;
        newNumber();
    };

    let answerInput = jQuery('#answer');

    answerInput.focus().on('keypress', function(event) {
        if (event.which !== 13) {
            return;
        }

        if (answerInput.val() === '') {
            return;
        }

        if (baseNumbers.length === 0) {
            return;
        }

        if (Learn.answersingLocked) {
            return;
        }

        Learn.answersingLocked = true;

        if (answerInput.val() === (currentNumber * currentMultiplier).toString()) {
            Learn.updateStats(true, currentNumber * currentMultiplier, newNumber);
        } else {
            Learn.updateStats(false, currentNumber * currentMultiplier, newNumber);
        }
    })

    let newNumber = function() {
        do {
            currentNumber = baseNumbers[Math.floor(Math.random() * baseNumbers.length)];
        } while (!calculations[currentNumber] || calculations[currentNumber].length === 0)

        let currentMultiplierIndex = Math.floor(Math.random() * calculations[currentNumber].length);
        currentMultiplier = calculations[currentNumber][currentMultiplierIndex];
        calculations[currentNumber].splice(currentMultiplierIndex, 1);
        if (calculations[currentNumber].length === 0) {
            calculations[currentNumber] = null;
            let empty = true;
            for (let i in baseNumbers) {
                if (!baseNumbers.hasOwnProperty(i)) {
                    continue;
                }

                if (calculations[baseNumbers[i]]) {
                    empty = false;
                    break;
                }
            }

            if (empty) {
                populateCalculations();
                Learn.completeAudio.play();
            }
        }

        question.html(`${currentMultiplier} x ${currentNumber} = `);
        Learn.answersingLocked = false;
    };
    populateCalculations();
}