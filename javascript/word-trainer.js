Learn.initializeWordLists = function () {
    let questions = [];
    let questionKey = 0;
    let questionContainer = jQuery('#question');
    let answerContainer = jQuery('#multiple-choice-answers');

    let groups = [];
    for (let i in Learn.wordLists) {
        if (!Learn.wordLists.hasOwnProperty(i)) {
            continue;
        }
        groups.push(`<span class="group" data-key="${i}">${i}</span>`);
    }
    let groupSelector = jQuery('#group-selector');
    groupSelector.html(groups.join(", "));
    groupSelector.find('.group').off().on('click', function () {
        groupSelector.find('.active').removeClass('active');
        let group = jQuery(this);
        group.addClass('active');
        let words = Learn.wordLists[group.data('key')];
        let lists = [];
        for (let i in words) {
            if (!words.hasOwnProperty(i)) {
                continue;
            }
            lists.push(`<span class="word-list" data-key="${i}">${i}</span>`);
        }
        Learn.save('wordListGroup', group.data('key'));
        let wordListSelector = jQuery('#word-lists');
        wordListSelector.html(lists.join(", "));
        wordListSelector.find('.word-list').off().on('click', function () {
            wordListSelector.find('.active').removeClass('active');
            let wordList = jQuery(this);
            wordList.addClass('active');
            let wordSelection = Learn.wordLists[group.data('key')][wordList.data('key')];
            populateQuestions(wordSelection);
            Learn.save('wordListSet', wordList.data('key'));
        });
    });

    let populateQuestions = function (words) {
        let answers = [];
        for (let i in words) {
            if (!words.hasOwnProperty(i)) {
                continue;
            }
            let word = words[i];
            answers.push(word[1]);
        }
        questions = [];
        questionKey = 0;
        for (let i in words) {
            if (!words.hasOwnProperty(i)) {
                continue;
            }
            let word = words[i];
            let options = [];
            let usedOptions = [];
            let numberOfOptions = 0;
            while (numberOfOptions < 4) {
                let number = Math.floor(Math.random() * answers.length);
                if (!usedOptions[number]) {
                    usedOptions[number] = true;
                    options.push(answers[number]);
                    numberOfOptions++;
                }
            }
            let answerIn = false;
            for (let i in options) {
                if (!options.hasOwnProperty(i)) {
                    continue;
                }
                if (options[i] === word[1]) {
                    answerIn = true;
                    break;
                }
            }
            if (!answerIn) {
                options[Math.floor(Math.random() * 4)] = word[1];
            }
            questions.push({
                question: word[0],
                answer: word[1],
                options: options
            });
        }
        Learn.answersPerSet = questions.length
        Learn.resetStats();
        questions = Learn.shuffle(questions);
        showQuestion();
    };

    let showQuestion = function () {
        if (!questions[questionKey]) {
            Learn.completeAudio.play();
            questionKey = 0;
        }

        let currentQuestion = questions[questionKey];

        questionContainer.html(currentQuestion.question);
        let answerMarkup = [];
        for (let i in currentQuestion.options) {
            if (!currentQuestion.options.hasOwnProperty(i)) {
                continue
            }
            answerMarkup.push(`<span class="answer" data-answer="${currentQuestion.options[i]}">${currentQuestion.options[i]}</span>`);
        }
        answerContainer.html(answerMarkup.join("<br/>\n"));
        answerContainer.find('.answer').off().on('click', function (event) {
            event.preventDefault();
            if (Learn.answersingLocked) {
                return;
            }
            Learn.answersingLocked = true;
            let answer = jQuery(this);
            if (answer.html() === currentQuestion.answer) {
                answer.addClass('correct');
                Learn.updateStats(true, '', nextQuestion);
            } else {
                answer.addClass('incorrect');
                answerContainer.find(`.answer[data-answer="${currentQuestion.answer}"]`).addClass('correct');
                Learn.updateStats(false, '', nextQuestion, true);
            }
        });
    };

    let nextQuestion = function () {
        questionKey++;
        showQuestion();
        Learn.answersingLocked = false;
    };

    let group = Learn.load('wordListGroup');
    if (group) {
        groupSelector.find(`[data-key="${group}"]`).click();
        let wordListSet = Learn.load('wordListSet');
        if (wordListSet) {
            jQuery('#word-lists').find(`[data-key="${wordListSet}"]`).click();
        }
    }
};