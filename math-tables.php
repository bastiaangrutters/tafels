<?php
include 'php/header.php';
?>
        <a href="index.php">terug</a><br/><br/>
        <div id="selection-container">
            <span class="title">Tafels:</span>
            <span class="toggle active" data-base="0">0</span>
            <span class="toggle active" data-base="1">1</span>
            <span class="toggle active" data-base="2">2</span>
            <span class="toggle active" data-base="3">3</span>
            <span class="toggle active" data-base="4">4</span>
            <span class="toggle active" data-base="5">5</span>
            <span class="toggle active" data-base="6">6</span>
            <span class="toggle active" data-base="7">7</span>
            <span class="toggle active" data-base="8">8</span>
            <span class="toggle active" data-base="9">9</span>
            <span class="toggle active" data-base="10">10</span>
        </div>
        <div id="main">
            <label><span id="question"></span> <input type="text" id="answer" /></label>
            <span id="correction" class="hidden"></span>
        </div>
<?php
include 'php/stats.php';
include 'php/footer.php';
?>
