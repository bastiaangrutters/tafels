	</div>
	<div id="footer"><a href="https://gitlab.com/bastiaangrutters/tafels/-/tree/master">Code</a></div>
	<script type="text/javascript" src="javascript/jquery.min.js"></script>
	<script type="text/javascript" src="javascript/main.js"></script>
	<script type="text/javascript" src="javascript/math-tables.js"></script>
	<script type="text/javascript" src="javascript/word-trainer.js"></script>
	<script type="text/javascript" src="javascript/word-lists.js"></script>
	<!-- Applause.wav from https://opengameart.org/content/applause used under CC BY 3.0 -->
	<!-- Other audio used is in public domain -->
</body>
</html>