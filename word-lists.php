<?php
include 'php/header.php';
?>
        <a href="index.php">terug</a><br/><br/>
        <div id="selection-container" class="word-list-selection">
            <span class="title">Maak je keuze:</span>
            <div id="group-selector"></div>
            <div id="word-lists"></div>
        </div>
        <div id="main">
            <label><span id="question"></span></label>
            <div id="multiple-choice-answers"></div>
            <span id="correction" class="hidden"></span>
        </div>
<?php
include 'php/stats.php';
include 'php/footer.php';
?>
